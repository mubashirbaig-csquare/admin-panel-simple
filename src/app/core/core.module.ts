import { NgModule } from '@angular/core';
import { HeaderComponent, SidebarComponent } from 'app/shared';
import { AppRoutingModule } from 'app/app-routing.module';

@NgModule({
    declarations: [
        HeaderComponent,
        SidebarComponent
    ],
    imports: [
        AppRoutingModule
    ],
    exports: [
        AppRoutingModule
    ]
})
export class CoreModule {}